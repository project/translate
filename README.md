== Introduction ==

Configures and installs Translate.com's Website Localizer code into your Drupal
site.

= You pick your languages, we translate your site =

* Automatically translates your site’s existing text and watches for future
* content updates as well. Detects visitor language settings and displays the
* appropriate translation. Customizable, mobile-friendly widget optionally gives
* your users even more language control. Manage and refine all your translations
* through your Translate.com portal. Boost your SEO with professional human
* translations at the click of a button.

= Automatic Translation AI + Post-Machine Editors  + Quality Assurance =

With us, automatic machine translation is just the beginning. Through your
customer portal you can easily submit all your site’s text or just your most
important phrases to our growing network of over 30,000 professional translators
from around the world, available 24/7.

Professional human translation is the only truly effective way to expand your
site to a global audience. No one makes this as easy and seamless as
Translate.com.

Get started today with a 30-day free trial on all paid plans.

= Questions? Our team is standing by to help! Get in touch at
= support@translate.com =

Translation available for: Afrikaans, Albanian, Arabic, Armenian, Azerbaijani,
Basque, Belarusian, Bengali, Bosnian (Latin), Bulgarian, Catalan, Cebuano,
Chinese (Simplified), Chinese (Traditional), Croatian, Czech, Danish, Dutch,
English, Esperanto, Estonian, Filipino, Finnish, French, Galician, Georgian,
German, Greek, Gujarati, Haitian Creole, Hausa, Hebrew, Hindi, Hmong, Hungarian,
Icelandic, Igbo, Indonesian, Irish, Italian, Japanese, Javanese, Kannada, Khmer,
Korean, Lao, Latin, Latvian, Lithuanian, Macedonian, Malay, Maltese, Maori,
Marathi, Mongolian, Nepali, Norwegian, Persian, Polish, Portuguese,, Punjabi,
Romanian, Russian, Serbian (Cyrillic), Slovak, Slovenian, Somali, Spanish,
Swahili, Swedish, Tamil, Telugu, Thai, Turkish, Ukrainian, Urdu, Vietnamese,
Welsh, Yiddish, Yoruba, Zulu, Hmong Daw, Klingon, Serbian (Latin).

== Pricing ==

= Free Plan - 2 Translation Languages = Price: Always free! You get: 10,000
monthly pageviews 200 Human Translation words + 100,000 monthly Machine
Translation words

= Starter Plan: 5 Translation Languages = Price: 30-Day Free Trial! ($59 / month
after) You get: 250,000 monthly pageviews 500 monthly Human Translation words +
150,000 monthly Machine Translation words

= Professional Plan: 15 Translation Languages = Price: 30-Day Free Trial! ($299
/ month after) You get: 2,500,000 monthly pageviews 2,500 monthly Human
Translation words + 750,000 monthly Machine Translation words

= Professional Plan: 15 Translation Languages = Price: 30-Day Free Trial! ($1199
/ month after) You get: 10,000,000 monthly pageviews 10,000 monthly Human
Translation words + 3,000,000 monthly Machine Translation words


== Installation == = To install: =

1. Open drupal admin panel. 2. Go to "Modules". Click on Install new module
button. (If you don't see install new module button then you need to
enable/install Update Manger from list below on that page.) 3. Select
translate-7.0.zip or translate-7.0.tar.gz file and upload it. 4. Go to "Modules"
and activate `Translate` module (under `Custom` category). 5. Go to
"Configuration > Web Services > Translate" and fill up the form to register your
site with Translate.com.

== Frequently Asked Questions ==

You can manage and refine your translations as well as adjust your settings
through your Customer Portal on [Translate.com](https://translate.com)

Full documentation for this plugin and the Website Localizer can be found at
[Help Center](https://www.translate.com/faq)
